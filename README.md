# [Nicolas-dmg.fr - Portfolio](https://nicolas-dmg.fr)

![Banner image](https://nicolas-dmg.fr/images/opengraph.png)

Full-Stack JS Developer with over 6 years of experience in building robust and performant web applications.
