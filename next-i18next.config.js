module.exports = {
  i18n: {
    defaultLocale: "fr",
    locales: ["bg", "de", "en", "fr"],
  },
};
