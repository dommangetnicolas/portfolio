import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { FunctionComponent } from "react";
import { DefaultLayout } from "../components/layout";
import ProjectOverview from "../components/pages/projects/ProjectOverview";
import OpenGraph from "../components/ui/OpenGraph";
import { projects } from "../data";

const Projects: FunctionComponent = () => {
  const { t } = useTranslation("projects");

  return (
    <DefaultLayout>
      <OpenGraph />

      <section className="projects">
        <div className="container container--pall">
          <div className="projects__intro">
            <h2>{t("title")}</h2>
            <p>{t("introduction")}</p>
          </div>

          <div className="projects__list">
            {projects.map((project) => (
              <ProjectOverview key={project.name.en} project={project} />
            ))}
          </div>
        </div>
      </section>
    </DefaultLayout>
  );
};

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "projects"])),
  },
});

export default Projects;
