import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Image from "next/image";
import { FunctionComponent, useEffect } from "react";
import DefaultLayout from "../components/layout/DefaultLayout";
import Articles from "../components/pages/index/Articles";
import ContactForm from "../components/pages/index/ContactForm/ContactForm";
import Projects from "../components/pages/index/Projects";
import ServiceApi from "../public/images/service-api.svg";
import ServiceMobile from "../public/images/service-mobile.svg";
import ServiceWeb from "../public/images/service-web.svg";
import Profile from "../public/images/profile.jpeg";
import OpenGraph from "../components/ui/OpenGraph";

const Index: FunctionComponent = () => {
  const { t } = useTranslation("home");

  return (
    <DefaultLayout>
      <OpenGraph />

      <section className="introduction" id="introduction">
        <div className="container">
          <div className="introduction__image">
            <div className="introduction__image-profile">
              <Image src={Profile} alt="Nicolas Dommanget-Muller" />
            </div>
          </div>

          <div className="introduction__text container--pall">
            <h1>{t("title")}</h1>
            <h2>Nicolas Dommanget</h2>
            <p className="pre">{t("introduction")}</p>
            <a href="#contact" className="button introduction__cta">
              {t("contactMe")}
            </a>
          </div>
        </div>
      </section>

      <section className="services">
        <div className="container container--pall">
          <div className="services__intro">
            <h2>{t("servicesTitle")}</h2>
            <p>{t("servicesIntroduction")}</p>
          </div>

          <div className="services__list">
            <div className="service">
              <div className="service__icon">
                <Image
                  src={ServiceMobile}
                  width="72"
                  height="72"
                  alt="Mobile Development"
                />
              </div>
              <div className="service__title">{t("serviceMobileTitle")}</div>
              <div className="service__description pre">
                {t("serviceMobileIntroduction")}
              </div>
            </div>

            <div className="service">
              <div className="service__icon">
                <Image
                  src={ServiceWeb}
                  width="72"
                  height="72"
                  alt="Web Development"
                />
              </div>
              <div className="service__title">{t("serviceWebTitle")}</div>
              <div className="service__description">
                {t("serviceWebIntroduction")}
              </div>
            </div>

            <div className="service">
              <div className="service__icon">
                <Image
                  src={ServiceApi}
                  width="72"
                  height="72"
                  alt="Api Development"
                />
              </div>
              <div className="service__title">{t("serviceAPITitle")}</div>
              <div className="service__description pre">
                {t("serviceAPIIntroduction")}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="projects">
        <div className="container container--pall">
          <h2>{t("projectsTitle")}</h2>

          <Projects />
        </div>
      </section>

      <section className="articles">
        <div className="container container--pall">
          <h2>{t("articlesTitle")}</h2>

          <Articles />
        </div>
      </section>

      <section className="contact" id="contact">
        <div className="container container--pall">
          <ContactForm />
        </div>
      </section>
    </DefaultLayout>
  );
};

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "home"])),
  },
});

export default Index;
