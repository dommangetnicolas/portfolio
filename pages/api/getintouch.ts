import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
  email: string;
  message: string;
};

const handler = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  if (req.method !== "POST") {
    return res.status(405).end();
  }

  const { name, email, message } = req.body;

  if (!name || !email || !message) {
    return res.status(422).end();
  }

  const data = {
    access_key: process.env.WEB3FORM_ACCESS_KEY,
    subject: "Form submition from nicolas-dmg.fr",
    name,
    email,
    message,
  };

  await fetch("https://api.web3forms.com/submit", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  return res.status(201).end();
};

export default handler;
