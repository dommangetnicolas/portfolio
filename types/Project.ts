import { StaticImageData } from "next/image";

export type Project = {
  name: {
    bg: string;
    de: string;
    en: string;
    fr: string;
  };
  description: {
    bg: string;
    de: string;
    en: string;
    fr: string;
  };
  technos: string[];
  thumbnail: StaticImageData;
  social?: {
    website?: string;
    playStore?: string;
    appStore?: string;
    gitlab?: string;
  };
};
