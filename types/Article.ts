export type Article = {
  thumbnail: string;
  pubDate: string;
  title: string;
  link: string;
};
