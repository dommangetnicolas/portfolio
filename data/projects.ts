import ProjectArmado from "../public/images/project-armado.png";
import ProjectDogmas from "../public/images/project-dogmas.png";
import ProjectFeels from "../public/images/project-feels.png";
import ProjectMailjet from "../public/images/project-mailjet.png";
import ProjectOneclinic from "../public/images/project-oneclinic.png";
import ProjectPresto from "../public/images/project-presto.png";
import ProjectShort from "../public/images/project-short.png";
import ProjectTravelBlog from "../public/images/project-travel-blog.png";
import ProjectYouWho from "../public/images/project-youwho.png";
import ProjectObat from "../public/images/project-obat.png";
import ProjectStravastreaks from "../public/images/project-stravastreaks.png";
import ProjectGobsly from "../public/images/project-gobsly.png";
import { Project } from "../types/Project";

const projects: Project[] = [
  {
    name: {
      bg: "Mailjet Email Editor",
      de: "Mailjet Email Editor",
      en: "Mailjet Email Editor",
      fr: "Mailjet Email Editor",
    },
    description: {
      bg: "Работа с екипа на Email редактора в Mailjet.com",
      de: "Zusammenarbeit mit dem E-Mail-Editor-Team bei Mailjet.com",
      en: "Worked with the email editor team at Mailjet.com",
      fr: "Collaboration avec l'équipe de l'éditeur d'e-mails chez Mailjet.com",
    },
    technos: ["React.js", "Node.js", "Golang", "TypeScript"],
    thumbnail: ProjectMailjet,
    social: {
      website: "https://mailjet.com/",
    },
  },
  {
    name: {
      bg: "Feels",
      de: "Feels",
      en: "Feels",
      fr: "Feels",
    },
    description: {
      bg: "Запознанства и приятели",
      de: "Dating & Friends",
      en: "Dating & Friends",
      fr: "Rencontres au feeling",
    },
    technos: [
      "React Native",
      "TypeScript",
      "Node.js",
      "PostgreSQL",
      "Storybook",
      "Fastlane",
    ],
    thumbnail: ProjectFeels,
    social: {
      website: "https://www.feels-app.com/",
      appStore:
        "https://apps.apple.com/us/app/feels-rencontres-au-feeling/id1455618069",
      playStore:
        "https://play.google.com/store/apps/details?id=com.feels&hl=en&gl=US",
    },
  },
  {
    name: {
      bg: "Obat",
      de: "Obat",
      en: "Obat",
      fr: "Obat",
    },
    description: {
      bg: "Софтуер за оферти и фактури онлайн за строителството.",
      de: "Online Angebots- und Rechnungssoftware für das Baugewerbe.",
      en: "Online quotation and invoice software for the construction industry.",
      fr: "Logiciel de devis et de facture en ligne pour le bâtiment.",
    },
    technos: ["React.js", "TypeScript"],
    thumbnail: ProjectObat,
    social: {
      website: "https://obat.fr/",
    },
  },
  {
    name: {
      bg: "Gobsly",
      de: "Gobsly",
      en: "Gobsly",
      fr: "Gobsly",
    },
    description: {
      bg: "Извикайте API и получете град, валута и други данни базирани на IP адрес.",
      de: "Rufen Sie eine API auf und erhalten Sie Stadt, Währung und mehr basierend auf der IP-Adresse.",
      en: "Call an API and get City, Currency, and more based on IP.",
      fr: "Appelez une API et obtenez la ville, la devise et d'autres informations basées sur l'adresse IP.",
    },
    technos: ["Next.js", "TypeScript", "Node.js", "Stripe", "Firebase"],
    thumbnail: ProjectGobsly,
    social: {
      website: "https://gobsly.com/",
      gitlab: "https://gitlab.com/gobsly/gobsly-location",
    },
  },
  {
    name: {
      bg: "Strava Streaks",
      de: "Strava Streaks",
      en: "Strava Streaks",
      fr: "Strava Streaks",
    },
    description: {
      bg: "Оценете последователността си в Strava с един поглед.",
      de: "Beurteilen Sie Ihre Strava-Konsistenz auf einen Blick.",
      en: "Assess your Strava consistency at a glance.",
      fr: "Évaluez votre régularité Strava en un coup d'œil.",
    },
    technos: ["Next.js", "TypeScript", "Firebase", "Vercel"],
    thumbnail: ProjectStravastreaks,
    social: {
      website: "https://stravastreaks.nicolas-dmg.fr/",
      gitlab: "https://gitlab.com/dommangetnicolas/strava-streaks",
    },
  },
  {
    name: {
      bg: "YouWho",
      de: "YouWho",
      en: "YouWho",
      fr: "YouWho",
    },
    description: {
      bg: "Социална медийна платформа",
      de: "Social media app",
      en: "Social media app",
      fr: "Réseau social",
    },
    technos: ["React Native", "AWS Amplify", "TypeScript"],
    thumbnail: ProjectYouWho,
    social: {
      website: "https://www.linkedin.com/company/youwhoapp/",
    },
  },
  {
    name: {
      bg: "Short (Side project)",
      de: "Short (Side project)",
      en: "Short (Side project)",
      fr: "Short (Side project)",
    },
    description: {
      bg: "Просто скъсяване на връзки",
      de: "A simple link shortener",
      en: "A simple link shortener",
      fr: "Écourteur de liens",
    },
    technos: ["React Native", "TypeScript"],
    thumbnail: ProjectShort,
    social: {
      gitlab: "https://gitlab.com/dommangetnicolas/short-app",
    },
  },
  {
    name: {
      bg: "Armado",
      de: "Armado",
      en: "Armado",
      fr: "Armado",
    },
    description: {
      bg: "Цифров трудов посредник",
      de: "Digital temporary work",
      en: "Digital temporary work",
      fr: "L'intérim digital",
    },
    technos: ["React Native", "Expo", "Fastlane", "TypeScript"],
    thumbnail: ProjectArmado,
    social: {
      website: "https://www.armado.fr/",
      appStore:
        "https://apps.apple.com/us/app/armado/id1380756494#?platform=iphone",
      playStore:
        "https://play.google.com/store/apps/details?id=com.armado.ArmadoMobile&hl=en&gl=US",
    },
  },
  {
    name: {
      bg: "Presto",
      de: "Presto",
      en: "Presto",
      fr: "Presto",
    },
    description: {
      bg: "Осигуряване на временни работници",
      de: "Provision of temporary workers",
      en: "Provision of temporary workers",
      fr: "Mise à disposition d'intérimaires",
    },
    technos: ["React Native", "Fastlane", "TypeScript"],
    thumbnail: ProjectPresto,
    social: {
      playStore:
        "https://play.google.com/store/apps/details?id=com.armado.prestomobile&hl=en&gl=US",
    },
  },
  {
    name: {
      bg: "OneClinic",
      de: "OneClinic",
      en: "OneClinic",
      fr: "OneClinic",
    },
    description: {
      bg: "Медицински кабинет в джоба ви",
      de: "A medical office in your pocket",
      en: "A medical office in your pocket",
      fr: "Un cabinet médical dans votre poche",
    },
    technos: ["Node.js", "React Native", "React.js", "TypeScript"],
    thumbnail: ProjectOneclinic,
    social: {
      website: "https://oneclinic.fr/",
      appStore:
        "https://apps.apple.com/us/app/one-clinic/id1534203492#?platform=iphone",
      playStore:
        "https://play.google.com/store/apps/details?id=fr.one.oneclinic&hl=fr&gl=FR",
    },
  },
  {
    name: {
      bg: "Dogmas.io",
      de: "Dogmas.io",
      en: "Dogmas.io",
      fr: "Dogmas.io",
    },
    description: {
      bg: "Забавно и без табута задаване на въпроси",
      de: "Making asking questions fun and without taboos",
      en: "Making asking questions fun and without taboos",
      fr: "Poser des questions funs et sans tabou",
    },
    technos: ["Node.js", "React Native", "React.js", "TypeScript"],
    thumbnail: ProjectDogmas,
  },
];

export default projects;
