import { FunctionComponent } from "react";

const Spinner: FunctionComponent = () => (
  <div className="ui-spinner">
    <div className="bounce1"></div>
    <div className="bounce2"></div>
    <div className="bounce3"></div>
  </div>
);

export default Spinner;
