import { FunctionComponent } from "react";

type Props = {
  title?: string;
  description?: string;
  image?: string;
  url?: string;
};

const OpenGraph: FunctionComponent<Props> = (props) => {
  const {
    title = "Nicolas Dommanget - Développeur Full-Stack (Node.js & React.js)",
    description = "Développeur Full-Stack JS avec plus de 6 ans d'expérience dans la création d'applications web robustes et performantes.",
    image = "/images/opengraph.png",
    url = "https://dev.nicolas-dmg.fr/",
  } = props;

  return (
    <>
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta property="og:url" content={url} />
      <meta property="og:type" content="website" />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content={url} />
      <meta name="twitter:creator" content="@nicolas_dmg" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
    </>
  );
};

export default OpenGraph;
