import { useTranslation } from "next-i18next";
import { FunctionComponent } from "react";
import { projects } from "../../../data";
import ProjectOverview from "../projects/ProjectOverview";

const Articles: FunctionComponent = () => {
  const { i18n } = useTranslation();

  const language = i18n.language as "en" | "fr";

  return (
    <div className="projects__list">
      {projects.slice(0, 3).map((project) => (
        <ProjectOverview key={project.name.en} project={project} />
      ))}
    </div>
  );
};

export default Articles;
