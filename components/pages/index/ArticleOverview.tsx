import React, { FunctionComponent } from "react";
import { Article } from "../../../types";
import { dayjs } from "../../../utils";

type Props = {
  item: Article;
};

const ArticleOverview: FunctionComponent<Props> = ({ item }) => (
  <a
    href={`${item.link}`}
    target="_blank"
    rel="noopener noreferrer"
    className="article__item"
  >
    <div
      className="article__image"
      style={{ backgroundImage: `url(${item.thumbnail})` }}
    ></div>

    <div className="article__text">
      <div className="article__author">{dayjs(item.pubDate).format("LL")}</div>
      <div className="article__title">{item.title}</div>
    </div>
  </a>
);

export default ArticleOverview;
