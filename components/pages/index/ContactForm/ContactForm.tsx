import { useTranslation } from "next-i18next";
import { ChangeEvent, FormEvent, FunctionComponent, useState } from "react";
import Spinner from "../../../ui/Spinner";
import { FormData } from "./Types";

const ContactForm: FunctionComponent = () => {
  const [form, setForm] = useState<FormData>({
    name: "",
    email: "",
    message: "",
  });
  const [isPending, setIsPending] = useState(false);
  const [isSent, setIsSent] = useState(false);

  const { t } = useTranslation("home");

  const onChange = ({
    target,
  }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setForm((prevState) => ({ ...prevState, [target.name]: target.value }));
  };

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      setIsPending(true);

      const url = `${window.location.protocol}//${window.location.host}/api/getintouch`;

      await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(form),
      });

      setIsSent(true);
    } finally {
      setIsPending(false);
    }
  };

  return (
    <form className="form" onSubmit={onSubmit}>
      <div className="form__container">
        {isSent && (
          <h3 className="form__success fade-in">{t("contactFormSent")}</h3>
        )}

        {!isSent && (
          <>
            <h2 className="form__title">{t("launchProject")}</h2>

            <input
              type="text"
              className="form__input"
              placeholder={t("contactFormName")}
              name="name"
              required
              value={form.name}
              onChange={onChange}
            />

            <input
              type="email"
              className="form__input input"
              placeholder={t("contactFormEmail")}
              name="email"
              required
              value={form.email}
              onChange={onChange}
            />

            <textarea
              className="form__input"
              rows={4}
              placeholder={t("contactFormMessage")}
              name="message"
              required
              minLength={30}
              value={form.message}
              onChange={onChange}
            ></textarea>

            <button type="submit" className="button button--hover-white form__button">
              {isPending && <Spinner />}
              {!isPending && t("contactFormSend")}
            </button>
          </>
        )}
      </div>
    </form>
  );
};

export default ContactForm;
