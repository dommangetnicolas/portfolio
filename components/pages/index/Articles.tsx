import React, { FunctionComponent, useEffect, useState } from "react";
import { Article } from "../../../types";
import ArticleOverview from "./ArticleOverview";

const Articles: FunctionComponent = () => {
  const [items, setItems] = useState<Article[]>([]);

  useEffect(() => {
    (async () => {
      const response = await fetch(
        "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2F%40nicolas-dmg"
      );

      const data = await response.json();
      if (!data.items) return;

      setItems(data.items.splice(0, 4));
    })();
  }, []);

  return (
    <div className="articles__list">
      {items.map((item) => (
        <ArticleOverview key={item.title} item={item} />
      ))}
    </div>
  );
};

export default Articles;
