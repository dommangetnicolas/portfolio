import Image from "next/image";
import { FunctionComponent } from "react";

type Props = {
  href: string;
  image: any;
  alt: string;
};

const ProjectLink: FunctionComponent<Props> = ({ href, image, alt }) => {
  return (
    <a href={href} target="_blank" rel="noopener noreferrer">
      <Image src={image} alt={alt} width="24" height="24" />
    </a>
  );
};

export default ProjectLink;
