import { useTranslation } from "next-i18next";
import { FunctionComponent } from "react";
import SocialAppStore from "../../../public/images/social-appstore.svg";
import SocialGitlab from "../../../public/images/social-gitlab.svg";
import SocialLink from "../../../public/images/social-link.svg";
import SocialPlayStore from "../../../public/images/social-playstore.svg";
import { Project } from "../../../types/Project";
import ProjectLink from "./ProjectLink";

type Props = {
  project: Project;
};

const ProjectOverview: FunctionComponent<Props> = ({ project }) => {
  const { thumbnail, name, description, technos, social } = project;

  const { i18n } = useTranslation();

  const language = i18n.language as "en" | "fr";

  return (
    <div className="project-overview">
      <div className="project-overview-content">
        <div
          className="project-overview__image"
          style={{ backgroundImage: `url(${thumbnail.src})` }}
        ></div>

        <div className="project-overview__text">
          <div className="project-overview__name">{name[language]}</div>
          <div className="project-overview__description">
            {description[language]}
          </div>

          <div>
            {technos.map((techno) => (
              <div key={techno} className="project-overview__techno badge">
                {techno}
              </div>
            ))}
          </div>

          <div className="project-overview__links">
            {social?.website && (
              <ProjectLink
                href={social.website}
                image={SocialLink}
                alt="Website"
              />
            )}
            {social?.playStore && (
              <ProjectLink
                href={social.playStore}
                image={SocialPlayStore}
                alt="Play Store"
              />
            )}
            {social?.appStore && (
              <ProjectLink
                href={social.appStore}
                image={SocialAppStore}
                alt="App Store"
              />
            )}
            {social?.gitlab && (
              <ProjectLink
                href={social.gitlab}
                image={SocialGitlab}
                alt="GitLab"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectOverview;
