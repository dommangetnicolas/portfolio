import { useTranslation } from "next-i18next";
import { useRouter } from "next/dist/client/router";
import {
  ChangeEvent,
  DetailedHTMLProps,
  FunctionComponent,
  SelectHTMLAttributes,
} from "react";

type Props = DetailedHTMLProps<
  SelectHTMLAttributes<HTMLSelectElement>,
  HTMLSelectElement
>;

const ChangeLanguage: FunctionComponent<Props> = (props) => {
  const { i18n } = useTranslation("common");
  const router = useRouter();

  const onChange = (e: ChangeEvent<HTMLSelectElement>) => {
    router.push(router.asPath, router.asPath, {
      locale: e.target.value,
      scroll: false,
    });
  };

  return (
    <select {...props} value={i18n.language} onChange={onChange}>
      <option value="bg">Български 🇧🇬</option>
      <option value="de">Deutsch 🇩🇪</option>
      <option value="fr">Français 🇫🇷</option>
      <option value="en">English 🇺🇸</option>
    </select>
  );
};

export default ChangeLanguage;
