import { FunctionComponent } from "react";
import Footer from "./Footer";
import Header from "./Header";

const DefaultLayout: FunctionComponent = ({ children }) => {
  return (
    <>
      <Header />
      <div style={{ paddingTop: "75px" }}>{children}</div>
      <Footer />
    </>
  );
};

export default DefaultLayout;
