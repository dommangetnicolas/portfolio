import { useTranslation } from "next-i18next";
import Image from "next/image";
import { FunctionComponent } from "react";
import SocialGitlab from "../../public/images/social-gitlab.svg";
import SocialLinkedin from "../../public/images/social-linkedin.svg";
import SocialMedium from "../../public/images/social-medium.svg";
import ChangeLanguage from "./ChangeLanguage";
import Link from "next/link";
import dayjs from "dayjs";

const Footer: FunctionComponent = () => {
  const { t } = useTranslation("common");

  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__social">
          <div className="icons">
            <a
              href="https://gitlab.com/dommangetnicolas"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={SocialGitlab} alt="GitLab" />
            </a>
            <a
              href="https://nicolas-dmg.medium.com/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={SocialMedium} alt="Medium" />
            </a>
            <a
              href="https://www.linkedin.com/in/nicolas-dommanget-muller/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={SocialLinkedin} alt="LinkedIn" />
            </a>
          </div>
        </div>

        <div className="footer__links">
          <Link href="/#introduction" passHref>
            <a href="">{t("about")}</a>
          </Link>
          <a
            href="https://nicolas-dmg.medium.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            {t("blog")}
          </a>
          <Link href="/projects" passHref>
            <a href="">{t("projects")}</a>
          </Link>
          <Link href="/#contact" passHref>
            <a href="">{t("contact")}</a>
          </Link>
        </div>

        <div className="footer__cta">
          <a
            href="https://go.nicolas-dmg.fr/call"
            target="_blank"
            rel="noopener noreferrer"
            className="button button--hover-white"
          >
            {t("scheduleCall")}
          </a>
        </div>
      </div>

      <div className="footer__bottom container">
        <ChangeLanguage className="language-select" />

        <div className="copyright">&copy; Nicolas-dmg.fr / 2019 - {dayjs().year()}</div>
      </div>
    </footer>
  );
};

export default Footer;
