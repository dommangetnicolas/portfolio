import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import {
  FunctionComponent,
  MouseEventHandler,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import Profile from "../../public/images/profile.jpeg";

const Header: FunctionComponent = () => {
  const [isOpen, setIsOpen] = useState<boolean | null>(null);

  const { t } = useTranslation("common");
  const mobileHeaderRef = useRef<HTMLDivElement>(null);

  const toggle = useCallback(() => setIsOpen((prevState) => !prevState), []);

  const onToggle: MouseEventHandler<HTMLAnchorElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    toggle();
  };

  useEffect(() => {
    const handleClick = (e: MouseEvent) => {
      const clickedOutside =
        mobileHeaderRef.current &&
        !mobileHeaderRef.current.contains(e.target as Node);

      if (clickedOutside && isOpen) toggle();
    };

    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [isOpen, mobileHeaderRef, toggle]);

  return (
    <header className={`header ${isOpen ? "open" : ""}`}>
      <div
        className={`
          overlay has-fade
          ${isOpen !== null && !isOpen ? "fade-out" : ""}
          ${isOpen ? "fade-in" : ""}
        `}
      ></div>

      <nav className="container container--pall flex flex-jc-sb flex-ai-c">
        <Link href="/" passHref>
          <a className="header__logo">
            <Image src={Profile} alt="Profile picture" width={40} height={40} />
          </a>
        </Link>

        <a
          id="btnHamburger"
          href="#"
          className="header__toggle desktop-hidden"
          onClick={onToggle}
        >
          <span></span>
          <span></span>
          <span></span>
        </a>

        <div className="header__links mobile-hidden">
          <Link href="/" passHref>
            {t("home")}
          </Link>
          <a
            href="https://nicolas-dmg.medium.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            {t("blog")}
          </a>
          <Link href="/projects" passHref>
            {t("projects")}
          </Link>
          <Link href="/#contact" passHref>
            {t("contact")}
          </Link>
        </div>
      </nav>

      <div
        ref={mobileHeaderRef}
        className={`
          header__menu has-fade 
          ${isOpen !== null && !isOpen ? "fade-out" : ""}
          ${isOpen ? "fade-in" : ""}
        `}
      >
        <Link href="/" passHref>
          <a onClick={toggle}>{t("home")}</a>
        </Link>
        <a
          href="https://nicolas-dmg.medium.com/"
          target="_blank"
          rel="noopener noreferrer"
          onClick={toggle}
        >
          {t("blog")}
        </a>
        <Link href="/projects" passHref>
          <a onClick={toggle}>{t("projects")}</a>
        </Link>
        <Link href="/#contact" passHref>
          <a onClick={toggle}>{t("contact")}</a>
        </Link>
      </div>
    </header>
  );
};

export default Header;
